//
//  File.swift
//  apiTest
//
//  Created by dd on 31/03/2018.
//  Copyright © 2018 dd. All rights reserved.
//

import Foundation


class Movie:Model
{
   
    
    
    
    private var fillable=["title","description","rate","image"];
    
    var title:String = ""
    var description:String = ""
    var rate:Int = 0
    var image:AnyObject? = nil
    required init?(from dictionary: Dictionary<String, Any>) {
        
        title = dictionary["title"] as! String
        description = dictionary["description"] as! String
        rate = dictionary["rate"] as! Int
        image = dictionary["image"] as! AnyObject?
        
    }
    
    /*
    
    required init?(from jsonObject: AnyObject) {
        
        self.title = jsonObject.object(forKey: "title") as? String
        self.description = jsonObject.object(forKey: "description") as? String
        self.rate = jsonObject.object(forKey: "rate") as? Int
        self.image = jsonObject.object(forKey: "image") as? String
        
    
    }
 */
    
}
