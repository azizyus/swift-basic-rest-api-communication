//
//  Model.swift
//  apiTest
//
//  Created by dd on 31/03/2018.
//  Copyright © 2018 dd. All rights reserved.
//


protocol Model {
    
     init?(from jsonObject: Dictionary<String,Any>) 
    
}
